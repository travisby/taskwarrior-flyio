FROM ubuntu AS builder
RUN apt-get update && \
  apt-get install -y cmake uuid-dev libgnutls28-dev gnutls-bin git g++ && \
  rm -rf /var/lib/apt/lists/* && \
  git clone https://github.com/GothenburgBitFactory/taskserver.git  && \
  cd taskserver && \
  sed -i 's/git.tasktools.org\/TM/github.com\/GothenburgBitFactory/' .git/config && \
  git submodule init && \
  git submodule update && \
  cmake -DCMAKE_BUILD_TYPE=release . && \
  make

FROM ubuntu AS certmanager
RUN apt-get update && \
  apt-get install -y gnutls-bin && \
  rm -rf /var/lib/apt/lists/*

COPY --from=builder taskserver/pki pki/
# use this like...
# podman run --rm -it -v ./certs/ca.key.pem:/pki/ca.key.pem:z -v ./certs/ca.cert.pem:/pki/ca.cert.pem:z -v ./vars:/pki/vars:z -v ./certs:/tmp/certs:z certmanager
# cd /pki/ && ./generate.client User_Name && cp User_Name* /tmp/certs/
# then put those in your ~/.task

FROM ubuntu

EXPOSE 53589
VOLUME /data
ENV TASKDDATA=/data

RUN useradd -d /app/ taskd && mkdir -p /app/ && chown -R taskd /app/
USER taskd
WORKDIR /app/

COPY --from=builder taskserver/src/taskd .
# we really only ened these two if we want to generate new certs
# COPY --from=builder --chown=taskd taskserver/pki pki/
# COPY --chown=taskd vars pki/
COPY --chmod=755 entrypoint.sh .

ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["server"]
