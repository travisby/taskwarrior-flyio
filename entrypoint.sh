#!/bin/bash

# our data directory did not exist; bootstrap
if ! test -f /data/config; then
    ./taskd init

    ./taskd config server 0.0.0.0:53589
    ./taskd config log -

    ./taskd config client.cert $(pwd)/pki/client.cert.pem
    ./taskd config client.key $(pwd)/pki/client.key.pem

    ./taskd config server.cert $(pwd)/pki/server.cert.pem
    ./taskd config server.key $(pwd)/pki/server.key.pem
    ./taskd config server.crl $(pwd)/pki/server.crl.pem

    ./taskd config ca.cert $(pwd)/pki/ca.cert.pem
fi

./taskd "$@"
